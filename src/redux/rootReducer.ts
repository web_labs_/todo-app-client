import {combineReducers} from 'redux';
import {todoReducer} from './todoReducer';
import {alertReducer} from './alertReducer';
import {signInReducer} from "./actions/signInReducer";
import {signUpReducer} from "./actions/signUpReducer";

export const rootReducer = combineReducers({
  todoReducer,
  alertReducer,
  signInReducer,
  signUpReducer,
})
