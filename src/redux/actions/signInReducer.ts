import {ISignTypes} from "../../types/types";

export interface ISignIn {
  type?: string,
  isAuthorized: boolean,
  status: number,
  message: string,
}

const initialState: ISignIn = {
  isAuthorized: !!localStorage.getItem('accessToken'),
  status: 0,
  message: '',
};

export const signInReducer = (state = initialState, action: ISignIn) => {
  switch (action.type) {
    case ISignTypes.SIGNIN_SUCCESS:
      return {
        ...state,
        isAuthorized: !!localStorage.getItem('accessToken'),
        status: action.status,
        message: action.message,
      };
    case ISignTypes.SIGNIN_ERROR:
      return {
        ...state,
        isAuthorized: !!localStorage.getItem('accessToken'),
        status: action.status,
        message: action.message,
      };
    default:
      return state;
  }
};
