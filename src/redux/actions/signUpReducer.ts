import {ISignTypes} from "../../types/types";

export interface ISignUp {
  type?: string,
  isAuthorized: boolean,
  status: number,
  message: string,
}

const initialState: ISignUp = {
  isAuthorized: !!localStorage.getItem('accessToken'),
  status: 0,
  message: '',
};

export const signUpReducer = (state = initialState, action: ISignUp) => {
  switch (action.type) {
    case ISignTypes.SIGNUP_SUCCESS:
      return {
        ...state,
        isAuthorized: !!localStorage.getItem('accessToken'),
        status: action.status,
        message: action.message,
      };
    case ISignTypes.SIGNUP_ERROR:
      return {
        ...state,
        isAuthorized: !!localStorage.getItem('accessToken'),
        status: action.status,
        message: action.message,
      };
    default:
      return state;
  }
};
