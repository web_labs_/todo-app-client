import {ISignTypes} from "../../types/types";
import {serverAddress} from "../../api";

const checkResponseStatus = (response: Response) => {
  if (response.ok) {
    return true;
  }
  throw new Error(response.status.toString());
};

export const signUpAction = (username: string, email: string, password: string) => async (
  dispatch: (arg: { type: string; status?: any; message?: string; }) => void,
) => {
  dispatch({
    type: ISignTypes.SIGNUP_PENDING,
  });
  let response: Response | undefined;
  let message;
  try {
    response = await fetch(
      `${serverAddress}/signup`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username, email, password}),
      }
    )
    message = await response.json();

    checkResponseStatus(response);
    localStorage.setItem('accessToken', message.accessToken);

    dispatch({
      type: ISignTypes.SIGNUP_SUCCESS,
      status: response.status,
    });
  } catch (e) {
    dispatch({
      type: ISignTypes.SIGNUP_ERROR,
      status: response?.status,
      message: message.message,
    });
    console.error(e);
  }
};
