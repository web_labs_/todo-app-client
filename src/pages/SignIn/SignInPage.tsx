import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Link, useNavigate} from 'react-router-dom';
import {signInAction} from "../../redux/actions/signInAction";
import {stateType} from "../../index";

import {Button, Form, Input} from 'antd';
import Text from 'antd/es/typography/Text';
import {appRoutes} from "../../config/appRoutes";

import './style.css';

const SignInPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [signForm] = Form.useForm();

  const isAuthorized = useSelector((state: stateType) => state.signInReducer.isAuthorized);

  const [loginIsEmpty, setLoginIsEmpty] = useState<boolean>(true);
  const [passwordIsEmpty, setPasswordIsEmpty] = useState<boolean>(true);
  const [errorIsVisible, setErrorIsVisible] = useState<boolean>(false);

  useEffect(() => {
    if (isAuthorized) {
      navigate(appRoutes.todo);
    }
  }, [isAuthorized, navigate]);

  const onFinish = (event: { email: string, password: string }): void => {
    setPasswordIsEmpty(true);
    setLoginIsEmpty(true);
    signForm.resetFields();
    dispatch<any>(signInAction(event.email, event.password));
    setErrorIsVisible(false);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>, func: (arg: boolean) => void): void => {
    func(event.target.value.trim().length === 0);
    setErrorIsVisible(false);
  };

  return (
    <div className="sign-wrap">
      <div className="sign-layout">
        <div className="sign-layout__title">Day Planner</div>
        <Form form={signForm} className="sign__main-form" onFinish={onFinish}>
          <Form.Item className="sign__login-item" name="email">
            <Input
              placeholder="Email"
              maxLength={50}
              onChange={(e) => handleChange(e, setLoginIsEmpty)}
              className={`sign__input ${errorIsVisible ? 'sign__input-error' : ''}`}
            />
          </Form.Item>
          <Form.Item className="sign__password-item" name="password">
            <Input.Password
              placeholder="Password"
              maxLength={30}
              onChange={(e) => handleChange(e, setPasswordIsEmpty)}
              className={`sign__input ${errorIsVisible ? 'sign__input-error' : ''}`}
            />
          </Form.Item>
            <Form.Item>
              <Button
                className="button-entity"
                disabled={loginIsEmpty || passwordIsEmpty}
                htmlType="submit"
              >
                Sign In
              </Button>
            </Form.Item>
        </Form>
        <div className="signin__register-text">
          <Text>Do not have an account yet?</Text>
          <a href={appRoutes.signup}>Register instead</a>
        </div>
      </div>
    </div>
  );
};

export default SignInPage;
