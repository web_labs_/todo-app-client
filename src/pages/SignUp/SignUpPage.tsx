import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Link, useNavigate} from 'react-router-dom';
import {signUpAction} from "../../redux/actions/signUpAction";
import {stateType} from "../../index";

import {Button, Form, Input} from 'antd';
import Text from 'antd/es/typography/Text';

import './style.css';
import {appRoutes} from "../../config/appRoutes";

const SignUpPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [signForm] = Form.useForm();

  const isAuthorized = useSelector((state: stateType) => state.signUpReducer.isAuthorized);

  const [nameIsEmpty, setNameIsEmpty] = useState<boolean>(true);
  const [loginIsEmpty, setLoginIsEmpty] = useState<boolean>(true);
  const [passwordIsEmpty, setPasswordIsEmpty] = useState<boolean>(true);
  const [errorIsVisible, setErrorIsVisible] = useState<boolean>(false);

  useEffect(() => {
    if (isAuthorized) {
      navigate(appRoutes.todo);
    }
  }, [isAuthorized, navigate]);

  const onFinish = (event: { username: string, email: string, password: string }): void => {
    setPasswordIsEmpty(true);
    setLoginIsEmpty(true);
    setNameIsEmpty(true);
    signForm.resetFields();
    dispatch<any>(signUpAction(event.username, event.email, event.password));
    setErrorIsVisible(false);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>, func: (arg: boolean) => void): void => {
    func(event.target.value.trim().length === 0);
    setErrorIsVisible(false);
  };

  return (
    <div className="sign-wrap">
      <div className="sign-layout">
        <div className="sign-layout__title">Day Planner</div>
        <Form form={signForm} className="sign__main-form" onFinish={onFinish}>
          <Form.Item className="sign__name-item" name="username">
            <Input
              placeholder="Username"
              maxLength={50}
              onChange={(e) => handleChange(e, setNameIsEmpty)}
              className={`sign__input ${errorIsVisible ? 'sign__input-error' : ''}`}
            />
          </Form.Item>
          <Form.Item className="sign__login-item" name="email">
            <Input
              placeholder="Email"
              maxLength={50}
              onChange={(e) => handleChange(e, setLoginIsEmpty)}
              className={`sign__input ${errorIsVisible ? 'sign__input-error' : ''}`}
            />
          </Form.Item>
          <Form.Item className="sign__password-item" name="password">
            <Input.Password
              placeholder="Password"
              maxLength={30}
              onChange={(e) => handleChange(e, setPasswordIsEmpty)}
              className={`sign__input ${errorIsVisible ? 'sign__input-error' : ''}`}
            />
          </Form.Item>
          <Form.Item>
            <Button
              className="button-entity"
              disabled={nameIsEmpty || loginIsEmpty || passwordIsEmpty}
              htmlType="submit"
            >
              Sign Up
            </Button>
          </Form.Item>
        </Form>
        <div className="signup__register-text">
          <Text>Already have an account?</Text>
          <a href={appRoutes.signin}>Sign In</a>
        </div>
      </div>
    </div>
  );
};

export default SignUpPage;
