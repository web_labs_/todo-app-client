import {applyMiddleware, compose, createStore} from 'redux';
import {Provider} from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import ReactDOM from 'react-dom/client';
import {rootReducer} from './redux/rootReducer';
import {sagaWatcher} from './redux/saga/sagas';
import App from './App';
import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import {appRoutes} from "./config/appRoutes";
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import 'antd/dist/antd.min.css';

import SignInPage from "./pages/SignIn/SignInPage";
import SignUpPage from "./pages/SignUp/SignUpPage";

const saga = createSagaMiddleware()

const store = createStore(rootReducer, compose(applyMiddleware(saga, thunk, logger)))

export type stateType = ReturnType<typeof store.getState>;

saga.run(sagaWatcher)

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <Provider store={store}>
    <BrowserRouter>
      <Routes>
        <Route path={appRoutes.todo} element={<App/>}/>
        <Route path={appRoutes.signin} element={<SignInPage/>}/>
        <Route path={appRoutes.signup} element={<SignUpPage/>}/>
        <Route path="*" element={<Navigate to={appRoutes.signin} replace/>}/>
      </Routes>
    </BrowserRouter>
  </Provider>
);
