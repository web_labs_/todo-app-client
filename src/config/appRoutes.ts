export const appRoutes = {
  todo: '/todo',
  signin: '/signin',
  signup: '/signup',
};
