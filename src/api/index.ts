import axios from 'axios';
import { ITodo, ITodoState } from '../types/types';

export const serverAddress = 'https://ekate-todo-app.herokuapp.com';

const headersObject = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
};

export class TodoApi {
    static async getTodos(): Promise<ITodoState[]> {
        const res = await axios.get(`${serverAddress}/todos`, { headers: headersObject });
        return res.data;
    }

    static async createTodo(todo: Partial<ITodo>): Promise<ITodoState[]> {
        const res = await axios.post(`${serverAddress}/todos`, todo, { headers: headersObject });
        return res.data;
    }

    static async deleteTodo(id: string): Promise<void> {
        await axios.delete(`${serverAddress}/todos/${id}`, { headers: headersObject });
    }

    static async completeTodo(todo: Partial<ITodo>): Promise<void> {
        await axios.patch(`${serverAddress}/todos/${todo.id}`, todo, { headers: headersObject });
    }

    static async editTodo(todo: Partial<ITodo>): Promise<ITodo> {
        const res = await axios.patch(`${serverAddress}/todos/${todo.id}`, todo, { headers: headersObject });
        return res.data
    }
}
