import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Header} from './components/Header/Header';
import {TodoForm} from './components/TodoForm/TodoForm';
import {TodoList} from './components/TodoList/TodoList';
import {getTodos} from './redux/actions';
import {stateType} from "./index";
import {useNavigate} from "react-router-dom";
import {appRoutes} from "./config/appRoutes";

function App() {
  const dispatch = useDispatch()
  const navigate = useNavigate();

  const isAuthorized = useSelector((state: stateType) => state.signInReducer.isAuthorized);

  useEffect(() => {
    if (!isAuthorized) {
      navigate(appRoutes.signin);
    }
  }, [isAuthorized, navigate]);

  useEffect(() => {
    dispatch(getTodos())
  }, [])

  return (
    <>
      <Header/>
      <main>
        <section>
          <div className="container pt-3">
            <TodoForm/>
            <h2 className='pt-3'>Новые дела</h2>
            <TodoList/>
          </div>
        </section>
      </main>
    </>
  );
}

export default App;
