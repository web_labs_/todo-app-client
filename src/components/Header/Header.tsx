import React from "react";
import {useNavigate} from "react-router-dom";
import {appRoutes} from "../../config/appRoutes";

import './style.css';

export const Header = () => {
  const navigate = useNavigate();

  const handleExitClick = () => {
    localStorage.removeItem('accessToken');
    window.location.reload();
    navigate(appRoutes.signin);
  }

  return (
    <header style={{backgroundColor: '#0071F7', color: 'white'}}>
      <div className="container pt-3 pb-3 main-header">
        <h1 className="main-header__title">Day Planner</h1>
        <div className="main-header__exit" onClick={handleExitClick}>Выход</div>
      </div>
    </header>
  )
}